from __future__ import division
from __future__ import print_function

import __builtin__  # Needed as bender replaces next
from collections import Counter, defaultdict
from itertools import tee

import numpy as np
try:
    from tqdm import tqdm
except ImportError:
    print('Try running "lb-run Bender/latest python -m pip install --user tqdm"')
    raise


# Does nothing but does keep my linter happy
if False:
    evt = None
    det = None
    run = None


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    __builtin__.next(b, None)
    return zip(a, b)


def print_output(all_classifications, n_events):
    # Clear some space on the screen to avoid the mess
    print('\n'*50)

    print('Using', n_events, 'events')
    for min_mc_hits, classifications in all_classifications.items():
        print('With a min MCHits cut of', min_mc_hits)
        for k, v in sorted(classifications.items()):
            print((' > '+k+':').ljust(30), v,
                  '({:.2%})'.format(v/np.sum(classifications.values())),
                  sep='\t')


base_path = '/dd/Structure/LHCb/BeforeMagnetRegion/VP/{side}/Module{module_id:02d}WithSupport/Module{module_id:02d}/Ladder{i}/Sensor{i}'
# This is a list of cut values which are considered
all_min_mc_hits = [0, 2, 3, 4, 5, 6, 10]
max_events = None

# Make a mapping of sensor ID -> sensor objects
sensDetID_to_sensor = {}
for module_id in range(52):
    side = {0: 'VPRight', 1: 'VPLeft'}[module_id % 2]
    for i in range(4):
        sensor = det[base_path.format(side=side, module_id=module_id, i=i)]
        assert sensor.sensorNumber() not in sensDetID_to_sensor
        sensDetID_to_sensor[sensor.sensorNumber()] = sensor

# Loop over all events
all_classifications = {n: Counter() for n in all_min_mc_hits}
progress_bar = tqdm(total=max_events)
try:
    while evt['/Event/Gen/Header']:
        # Group the MCHits by the MCParticle that made them
        mcparticlekey_to_hits = defaultdict(list)
        for hit in evt['/Event/pSim/VP/Hits'].get_data():
            mcparticlekey_to_hits[hit.mcParticle].append(hit)

        # Sanity check - The particle referenced by the MCHit should always be in /Event/MC/Particles
        all_particle_keys = [p.key() for p in evt['/Event/MC/Particles']]
        for key in mcparticlekey_to_hits:
            assert key in all_particle_keys, key

        # Group the MCParticles by their name
        name_to_particles = defaultdict(list)
        for particle in evt['/Event/MC/Particles']:
            name_to_particles[particle.name()].append(particle)

        # Look at the pions
        pions = name_to_particles['pi+'] + name_to_particles['pi-']

        for min_mc_hits, classifications in all_classifications.items():
            for pion in pions:
                # Look at the particles by time
                sorted_hits = sorted(mcparticlekey_to_hits[pion.key()], key=lambda h: h.tof)
                if len(sorted_hits) < min_mc_hits:
                    continue

                if len(sorted_hits) == 0:
                    classifications['No MCHits'] += 1
                    continue

                if len(sorted_hits) == 1:
                    classifications['Only 1 MCHit'] += 1
                    continue

                # Check if particles have changed direction
                module_ids = [sensDetID_to_sensor[h.sensDetID].module() for h in sorted_hits]
                directions = np.sign(np.diff(module_ids))
                directions = directions[directions != 0]
                if len(directions) >= 2:
                    # assert all(directions[0] == directions), (module_ids, directions)
                    if not all(directions[0] == directions):
                        classifications['Changed direction'] += 1
                        continue

                # Look how many times we change from left to right
                is_left = [sensDetID_to_sensor[hit.sensDetID].isLeft() for hit in sorted_hits]
                assert len(set(is_left)) in [0, 1, 2], is_left
                if len(set(is_left)) >= 1:
                    is_left = [is_left[0]] + [b for a, b in pairwise(is_left) if a != b]
                    classifications['Changes side {:02d} times'.format(len(is_left)-1)] += 1
                    continue
                classifications['No overlap'] += 1

        run(1)
        progress_bar.update(1)
        if max_events and progress_bar.n > max_events:
            print('Reached maximum number of events:', max_events)
            break
except:
    # Catch all exceptions to make sure output is always printed
    print_output(all_classifications, progress_bar.n)
    # Re-raise the original exception
    raise
else:
    print_output(all_classifications, progress_bar.n-1)

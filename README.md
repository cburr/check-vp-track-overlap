# Overlap checking in the VP

This script classifies particles by how many times they change from the A side to the C side of the Upgrade Velo.
This is done by looking at all `MCParticle`s in each event that are a charged pion.
The `MCHits` made by each particle are then found in `/Event/pSim/VP/Hits`.

## Input data

Example minimum bias events generated in Upgrade conditions by Mark Williams:

```
/lhcb/user/m/mrwillia/2016_11/145690/145690542/Boole-Extended.digi
/lhcb/user/m/mrwillia/2016_11/145690/145690543/Boole-Extended.digi
/lhcb/user/m/mrwillia/2016_11/145690/145690545/Boole-Extended.digi
/lhcb/user/m/mrwillia/2016_11/145690/145690549/Boole-Extended.digi
```

Find the PFN for your file:

```
$ lb-run LHCbDIRAC/prod lhcb-proxy-init
$ lb-run LHCbDIRAC/prod dirac-dms-lfn-accessURL /lhcb/user/m/mrwillia/2016_11/145690/145690542/Boole-Extended.digi

Successful :
    GRIDKA-USER :
        /lhcb/user/m/mrwillia/2016_11/145690/145690542/Boole-Extended.digi : root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/user/m/mrwillia/2016_11/145690/145690542/Boole-Extended.digi
```

## Running

Ensure you have a grid proxy then run with one (or more) PFNs:

```
git clone ssh://git@gitlab.cern.ch:7999/cburr/check-vp-track-overlap.git
cd check-vp-track-overlap
lb-run Bender/latest bender --batch --simulation --datatype Upgrade check_overlaps.py \
    root://f01-080-123-e.gridka.de:1094/pnfs/gridka.de/lhcb/user/m/mrwillia/2016_11/145690/145690542/Boole-Extended.digi
```
